﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPractica.AppCode.Models
{
    public class Ventas
    {
        public int id { get; set; }
        public string nit { get; set; }
        public decimal total { get; set; }
        public int cantidad { get; set; }
        public string fecha_create { get; set; }
        
    }
}