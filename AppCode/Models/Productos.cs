﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPractica.AppCode.Models
{
    public class Productos
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int cantidad {get; set;}
        public decimal precio { get; set; }
        public int proveedor { get; set; }
        public int sucursal { get; set; }
        public string nproveedor { get; set; }
        public string nsucursal { get; set; }

        
        
    }
}