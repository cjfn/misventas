﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPractica.AppCode.Models
{
    public class Sucursales
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
    }
}