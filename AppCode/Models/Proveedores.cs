﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPractica.AppCode.Controllers;

namespace WebPractica.AppCode
{
    public class Proveedores
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }

    }
}