﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPractica.AppCode.Models
{
    public class DetalleVentas
    {
        public int id { get; set; }
        public int factura { get; set; }
        public int produ { get; set; }
        public int cantidad { get; set; }
        public decimal subtotal { get; set; }
        public string fecha_create { get; set; }
        public string prod { get; set; }
    }
}