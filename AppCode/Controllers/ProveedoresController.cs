﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;



namespace WebPractica.AppCode.Controllers
{
    public class ProveedoresController
    {
        public string cs = ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString;

        public List<Proveedores> GetAllUsuarios()
        {
            List<Proveedores> listproveedores = new List<Proveedores>();


            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "allPROVE";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Proveedores provee = new Proveedores();
                    provee.id = Convert.ToInt32(rdr["id"]);
                    provee.nombre = rdr["nombre"].ToString();
                    provee.telefono = rdr["telefono"].ToString();
                    provee.direccion = rdr["direccion"].ToString();
                    provee.email = rdr["email"].ToString();
                    listproveedores.Add(provee);

                }
                return listproveedores;
            }
        }
           
            public void AddUsuario(string nombre, string direccion, int telefono, string email)
        {
           
            using (SqlConnection con = new SqlConnection(cs))
            {
                int id = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "PROVE_CRUD";
                cmd.Parameters.AddWithValue("@Action", "INSERT");
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

            public void UpdateUsuario(int id, string nombre, string direccion, string telefono, string email)
            {
                int tel = Convert.ToInt32(telefono);
                using (SqlConnection con = new SqlConnection(cs))
                {
                    using (SqlCommand cmd = new SqlCommand("PROVE_CRUD"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Action", "UPDATE");
                        cmd.Parameters.AddWithValue("@id", id);
                        cmd.Parameters.AddWithValue("@nombre", nombre);
                        cmd.Parameters.AddWithValue("@direccion", direccion);
                        cmd.Parameters.AddWithValue("@email", email);
                        cmd.Parameters.AddWithValue("@telefono", tel);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
               
            }

            public void DeleteUsuario(int id)
            {
                
                using (SqlConnection con = new SqlConnection(cs))
                {
                    string nombre = "";
                    string direccion = "";
                    string email = "";
                    string tel = "";
                    using (SqlCommand cmd = new SqlCommand("PROVE_CRUD"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Action", "DELETE");
                        cmd.Parameters.AddWithValue("@id", id);
                        cmd.Parameters.AddWithValue("@nombre", nombre);
                        cmd.Parameters.AddWithValue("@direccion", direccion);
                        cmd.Parameters.AddWithValue("@email", email);
                        cmd.Parameters.AddWithValue("@telefono", tel);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }







     
    }
}