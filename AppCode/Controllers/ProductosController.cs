﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WebPractica.AppCode.Models;


namespace WebPractica.AppCode.Controllers
{
    public class ProductosController
    {

        public string cs = ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString;
        
        public Productos prod;
        public decimal precio;
        public string nombre;

        public List<Productos> GetAllProductos()
        {
            List<Productos> listproductos = new List<Productos>();


            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_MSSel_PRODU";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Productos prod = new Productos();
                    prod.id = Convert.ToInt32(rdr["id"]);
                    prod.nombre = rdr["nombre"].ToString();
                    prod.cantidad =Convert.ToInt32(rdr["cantidad"]);
                    prod.precio = Convert.ToDecimal(rdr["precio"]);
                    prod.proveedor = Convert.ToInt32(rdr["proveedor"]);
                    prod.sucursal = Convert.ToInt32(rdr["sucursal"]);
                    prod.nproveedor = rdr["nproveedor"].ToString();
                    prod.nsucursal = rdr["nsucursal"].ToString();
                    listproductos.Add(prod);

                }
                return listproductos;
            }
        }

        public List<Sucursales> GetAllSucu()
        {
            List<Sucursales> listsucu = new List<Sucursales>();


            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "allSUCUR";
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Sucursales provee = new Sucursales();
                    provee.id = Convert.ToInt32(rdr["id"]);
                    provee.nombre = rdr["nombre"].ToString();
                    provee.telefono = rdr["telefono"].ToString();
                    provee.direccion = rdr["direccion"].ToString();
                    provee.email = rdr["email"].ToString();
                    listsucu.Add(provee);

                }
                return listsucu;
            }
        }

        public List<Productos> GetProdbyId(int id)
        {

            List<Productos> listprod = new List<Productos>(id);
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_MSSelId_PRODU";
                cmd.Parameters.AddWithValue("@id", id);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Productos prod = new Productos();
                    prod.id = Convert.ToInt32(rdr["id"]);
                    prod.nombre = rdr["nombre"].ToString();
                    prod.cantidad = Convert.ToInt32(rdr["cantidad"]);
                    prod.precio = Convert.ToDecimal(rdr["precio"]);
                    prod.proveedor = Convert.ToInt32(rdr["proveedor"]);
                    prod.sucursal = Convert.ToInt32(rdr["sucursal"]);
                    listprod.Add(prod);
                }
                return listprod;
            }
        }

        public void GetProdsbyId( int id)
        {

            List<Productos> listprod = new List<Productos>(id);
            
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_MSSelId_PRODU";
                cmd.Parameters.AddWithValue("@id", id);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    prod = new Productos();
                    prod.id = Convert.ToInt32(rdr["id"]);
                    prod.nombre = rdr["nombre"].ToString();
                    prod.cantidad = Convert.ToInt32(rdr["cantidad"]);
                    prod.precio = Convert.ToDecimal(rdr["precio"]);
                    prod.proveedor = Convert.ToInt32(rdr["proveedor"]);
                    prod.sucursal = Convert.ToInt32(rdr["sucursal"]);
                    nombre = rdr["nombre"].ToString();
                    precio = Convert.ToDecimal(rdr["precio"]);
                    listprod.Add(prod);
                }
                
            }
        }
        public int deleteProductos(int id)
        {
            
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "PRODU_CRUD";
                cmd.Parameters.AddWithValue("@Action", "DELETE");
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", "");
                cmd.Parameters.AddWithValue("@cantidad", 0);
                cmd.Parameters.AddWithValue("@precio", 0.00);
                cmd.Parameters.AddWithValue("@proveedor", 0);
                cmd.Parameters.AddWithValue("@sucursal", 0);
                cmd.Connection = con;
                con.Open();
                int result = cmd.ExecuteNonQuery();
                con.Close();
                return result;
            }
            

        }

        public int updateProductos(int id, string nombre, int cantidad, decimal precio, int proveedor, int sucursal)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "PRODU_CRUD";
                cmd.Parameters.AddWithValue("@Action", "UPDATE");
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.Parameters.AddWithValue("@precio", precio);
                cmd.Parameters.AddWithValue("@proveedor", proveedor);
                cmd.Parameters.AddWithValue("@sucursal", sucursal);
                cmd.Connection = con;
                con.Open();
                int result = cmd.ExecuteNonQuery();
                con.Close();
                return result;
            }
        }  

        public int insertProductos(string nombre, int cantidad, decimal precio, int proveedor, int sucursal)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "PRODU_CRUD";

                cmd.Parameters.AddWithValue("@Action", "INSERT");
                cmd.Parameters.AddWithValue("@id", "0");
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.Parameters.AddWithValue("@precio", precio);
                cmd.Parameters.AddWithValue("@proveedor", proveedor);
                cmd.Parameters.AddWithValue("@sucursal", sucursal);
                cmd.Connection = con;
                con.Open();
                int result = cmd.ExecuteNonQuery();
                con.Close();
                return result;
                
            }
            
        }

        

    }
}