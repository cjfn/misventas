﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using WebPractica.AppCode.Models;

namespace WebPractica.AppCode.Controllers
{
    public class VentaController
    {
        public string cs = ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString;
        public int factura;

        public int newVenta(string nit)
        {
            
            List<Ventas> listvta = new List<Ventas>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_MSins_SALES";
                cmd.Parameters.AddWithValue("@nit", nit);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Ventas ven = new Ventas();
                    ven.id = Convert.ToInt32(rdr["id"]);
                    ven.nit = rdr["nit"].ToString();
                    ven.cantidad = Convert.ToInt32(rdr["cantidad"]);
                    ven.total = Convert.ToDecimal(rdr["total"]);
                    ven.fecha_create = (rdr["fecha_create"].ToString());
                    listvta.Add(ven);
                    factura=Convert.ToInt32(rdr["id"]);
                }
                return factura;
            }
        }

        public List<DetalleVentas> DetalleVenta(int factura)
        {

            List<DetalleVentas> listvta = new List<DetalleVentas>();
            using (SqlConnection con = new SqlConnection(cs))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_MSgetid_SALE";
                cmd.Parameters.AddWithValue("@factura", factura);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DetalleVentas ven = new DetalleVentas();
                    ven.id = Convert.ToInt32(rdr["id"]);
                    ven.prod = rdr["prod"].ToString();
                    ven.produ = Convert.ToInt32(rdr["produ"]);
                    ven.cantidad = Convert.ToInt32(rdr["cantidad"]);
                    ven.subtotal = Convert.ToDecimal(rdr["subtotal"]);
                    listvta.Add(ven);
                }
                return listvta;
            }
        }

        public int nuevoDetalle(int factura, int producto, int cantidad, decimal subtotal)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_MSins_SALED";
                cmd.Parameters.AddWithValue("@factura", factura);
                cmd.Parameters.AddWithValue("@produ", producto);
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.Parameters.AddWithValue("@subtotal", subtotal);
                cmd.Parameters.AddWithValue("@result", 0);
                con.Open();
                int result = cmd.ExecuteNonQuery();
                con.Close();
                return result;
            }
        }
    }
}