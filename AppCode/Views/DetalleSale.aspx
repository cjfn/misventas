﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetalleSale.aspx.cs" Inherits="WebPractica.AppCode.Views.DetalleSale" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <table>
            <tr>
                <th>Producto:<asp:TextBox ID="txtproducto" runat="server"></asp:TextBox><asp:Button ID="Button2" runat="server" Text="Buscar" OnClick="Button2_Click" /></th>
                <th>Precio<asp:Label ID="lblprecio" runat="server" Text="0.00"></asp:Label></th>
                <th>Descripcion<asp:Label ID="lbldescripcion" runat="server" Text=""></asp:Label></th>
                <th>Cantidad<asp:TextBox ID="txtcantidad" runat="server" OnDataBinding="Page_Load" ></asp:TextBox></th>
           </tr>
            <tr>
                <th>Descuento<asp:TextBox ID="txtdescuento" runat="server"></asp:TextBox><asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="CALCULAR" /></th>
                <th>Subtotal<asp:Label ID="lblsubtotal" runat="server" Text="0.00"></asp:Label></th>
                <th>
                    <asp:Button ID="Button1" runat="server" Text="Agregar" OnClick="Button1_Click" /></th>
            </tr>
        </table>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="id"
        OnRowDataBound="OnRowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" EmptyDataText="No records has been added.">
        <Columns>
            <asp:TemplateField HeaderText="id" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtid" runat="server" Text='<%# Eval("id") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="nombre" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblnombre" runat="server" Text='<%# Eval("prod") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtnombre" runat="server" Text='<%# Eval("prod") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="cantidad" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblcantidad" runat="server" Text='<%# Eval("produ") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtcantidad" runat="server" Text='<%# Eval("produ") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="precio" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblprecio" runat="server" Text='<%# Eval("subtotal") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtprecio" runat="server" Text='<%# Eval("subtotal") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            
            <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150"/>
        </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
