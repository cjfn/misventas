﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPractica.AppCode.Controllers;

namespace WebPractica.AppCode.Views
{
    public partial class Productos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
               LoadGrid();
               LoadProveedor();
               LoadSucur();
            }

        }

        protected void LoadGrid()
        {
            ProductosController pcr = new ProductosController();
            GridView1.DataSource = pcr.GetAllProductos();
            GridView1.DataBind();
        }

        protected void LoadProveedor()
        {
            ProveedoresController pr = new ProveedoresController();
            ddlproveedor.DataSource = pr.GetAllUsuarios();
            ddlproveedor.DataValueField = "id";
            ddlproveedor.DataTextField = "nombre";
            ddlproveedor.DataBind();
        }

        protected void LoadSucur()
        {
            ProductosController pr = new ProductosController();
            ddlsucursal.DataSource = pr.GetAllSucu();
            ddlsucursal.DataValueField="id";
            ddlsucursal.DataTextField = "nombre";
            ddlsucursal.DataBind();
        }



        protected void OnRowCancelingEdit(object sender, EventArgs e)
        {
            GridView1.EditIndex = -1;
            LoadGrid();
        }

        protected void Insert(object sender, EventArgs e)
        {
            ProductosController prd = new ProductosController();
            if (prd.insertProductos(txtNombre.Text, Convert.ToInt32(txtCantidad.Text), Convert.ToDecimal(txtPrecio.Text), Convert.ToInt32(ddlproveedor.SelectedValue.ToString()), Convert.ToInt32(ddlsucursal.SelectedValue.ToString())) == 0)
            {
                Response.Write("<script type='javascript'>alert('ingreso realizado exitosamente');</script>");
                LoadGrid();
            }
            else
            {
                Response.Write("<script type='javascript'>alert('ingreso realizado mal');</script>");
            }
            


        }

        protected void OnRowDataBound(Object sender, EventArgs e)
        {

        }

        protected void OnRowEditing(Object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            LoadGrid();
            LoadProveedor();
            LoadSucur();
        }

        protected void OnRowUpdating(Object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            string nombre = (row.FindControl("txtNombre") as TextBox).Text;
            int cantidad = Convert.ToInt32((row.FindControl("txtCantidad") as TextBox).Text);
            decimal precio = Convert.ToDecimal((row.FindControl("txtprecio") as TextBox).Text);
            int proveedor = Convert.ToInt32((row.FindControl("txtproveedor") as TextBox).Text);
            int sucursal = Convert.ToInt32((row.FindControl("txtsucursal") as TextBox).Text);
            ProductosController prc = new ProductosController();
            
            prc.updateProductos(id, nombre, cantidad, precio, proveedor, sucursal);
            Response.Write("<script type='javascript'>alert('ingreso realizado exitosamente');</script>");
            LoadGrid();

        }
        protected void OnRowDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            ClientScriptManager CSM = Page.ClientScript;
           
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            ProductosController prc = new ProductosController();
            prc.deleteProductos(id);
            LoadGrid();
           
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int cod = Convert.ToInt32(txtbusca.Text);
            ProductosController pr = new ProductosController();
            GridView1.DataSource = pr.GetProdbyId(cod);
            GridView1.DataBind();
        }

        protected void ddlproveedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}