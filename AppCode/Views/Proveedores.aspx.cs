﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPractica.AppCode.Controllers;
using System.Configuration;

namespace WebPractica.AppCode.Views
{
    public partial class Proveedores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            LoadGrid();

        }


        private void LoadGrid()
        {
            ProveedoresController prv = new ProveedoresController();
            GridView1.DataSource = prv.GetAllUsuarios();
            GridView1.DataBind();
        }

        protected void OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            LoadGrid();
        }

        protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            string nombre = (row.FindControl("txtNombre") as TextBox).Text;
            string direccion = (row.FindControl("txtDireccion") as TextBox).Text;
            string telefono = (row.FindControl("txtTelefono") as TextBox).Text;
            string email = (row.FindControl("txtEmail") as TextBox).Text;
            ProveedoresController ptb = new ProveedoresController();
            ptb.UpdateUsuario(id, nombre, direccion, telefono, email);
            GridView1.EditIndex = -1;
            LoadGrid();
            Response.Write("<script language=javascript>alert('Datos modificados con exito');</script>");
        }
        

         protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            
             if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != GridView1.EditIndex)
            {
                //(e.Row.Cells[2].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
             
            //Response.Write("<script language=javascript>alert('hola');</script>");
        }

         protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
         {
             

             int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
             ProveedoresController ptb = new ProveedoresController();
             ptb.DeleteUsuario(id);
             LoadGrid();
         }

         protected void OnRowCancelingEdit(object sender, EventArgs e)
         {
             GridView1.EditIndex = -1;
             LoadGrid();
         }

         protected void Insert(object sender, EventArgs e)
         {
             ProveedoresController ptb = new ProveedoresController();
             ptb.AddUsuario(txtNombre.Text, txtDireccion.Text, int.Parse(txtTelefono.Text), txtEmail.Text);
             LoadGrid();
         }





    }
}