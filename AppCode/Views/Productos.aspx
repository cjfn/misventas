﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="WebPractica.AppCode.Views.Productos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <center>
        <asp:Label ID="Label1" runat="server" Text="Ingrese codigo de producto:"></asp:Label>
        <asp:TextBox ID="txtbusca" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Buscar" OnClick="Button1_Click" />
        </center>
     <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="id"
        OnRowDataBound="OnRowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" EmptyDataText="No records has been added.">
        <Columns>
            <asp:TemplateField HeaderText="id" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtid" runat="server" Text='<%# Eval("id") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="nombre" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblnombre" runat="server" Text='<%# Eval("nombre") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtnombre" runat="server" Text='<%# Eval("nombre") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="cantidad" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblcantidad" runat="server" Text='<%# Eval("cantidad") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtcantidad" runat="server" Text='<%# Eval("cantidad") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="precio" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblprecio" runat="server" Text='<%# Eval("precio") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtprecio" runat="server" Text='<%# Eval("precio") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="proveedor" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblproveedor" runat="server" Text='<%# Eval("nproveedor") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlproveedor" runat="server" Width="140"></asp:DropDownList>
                    <asp:TextBox ID="txtproveedor" runat="server" Text='<%# Eval("proveedor") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="sucursal" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblsucursal" runat="server" Text='<%# Eval("nsucursal") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtsucursal"  runat="server" Text='<%# Eval("sucursal") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150"/>
        </Columns>
        </asp:GridView>
        <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
        <tr>
            <td style="width: 150px">
                Nombre:<br />
                <asp:TextBox ID="txtNombre" runat="server" Width="140" />
            </td>
            <td style="width: 150px">
                Cantidad:<br />
                <asp:TextBox ID="txtCantidad" runat="server" Width="140" />
            </td>
            <td style="width: 150px">
                Precio:<br />
                <asp:TextBox ID="txtPrecio" runat="server" Width="140" />
            </td>
            <td style="width: 150px">
                Proveedor:<br />
                 <asp:DropDownList ID="ddlproveedor" runat="server" Width="140" OnSelectedIndexChanged="ddlproveedor_SelectedIndexChanged"></asp:DropDownList>
            </td>
             <td style="width: 150px">
                Sucursal:<br />
                
                 <asp:DropDownList ID="ddlsucursal" runat="server" Width="140"></asp:DropDownList>
            </td>

            <td style="width: 100px">
                <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Insert" />
            </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
