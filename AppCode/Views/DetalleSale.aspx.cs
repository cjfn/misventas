﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPractica.AppCode.Controllers;

namespace WebPractica.AppCode.Views
{
    public partial class DetalleSale : System.Web.UI.Page
    {
        public int fact;
        public int cod;
        public int cantidad;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            if (Convert.ToInt32(this.Request.QueryString.Get(0)) == 0)
                {
                    Response.Redirect("Sale.aspx");
                }
            else
                {
                    fact = Convert.ToInt32(this.Request.QueryString.Get(0));
                    LoadDetalle(fact);
                }
        }

        protected void LoadDetalle(int factura)
        {
            VentaController vn = new VentaController();
            GridView1.DataSource = vn.DetalleVenta(factura);
            GridView1.DataBind();
        }

        protected void OnRowDataBound(object sender, EventArgs e)
        {

        }

        protected void OnRowEditing(object sender, EventArgs e)
        {

        }

        protected void OnRowCancelingEdit(object sender, EventArgs e)
        {

        }

        protected void OnRowUpdating(object sender, EventArgs e)
        {

        }

        protected void OnRowDeleting(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            
            
            cod=Convert.ToInt32(txtproducto.Text);
            ProductosController prd = new ProductosController();
            prd.GetProdsbyId(cod);
            lblprecio.Text =prd.precio.ToString();
            lbldescripcion.Text = prd.nombre.ToString();
            
            //lbldescripcion.Text = prd.prod.nombre.ToString();
            
            

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            cantidad = Convert.ToInt32(txtcantidad.Text);
            decimal precio = Convert.ToDecimal(lblprecio.Text);
            decimal total = cantidad * precio;
            lblsubtotal.Text = Convert.ToString(total);
            decimal descuento = Convert.ToDecimal(txtdescuento.Text);
            decimal preciodesuento = descuento * precio;
            lblsubtotal.Text = Convert.ToString(preciodesuento);
        }

      
        protected void Button1_Click(object sender, EventArgs e)
        {
            int factura = fact;

            if (factura == 0 || Convert.ToInt32(txtproducto.Text) == 0 || Convert.ToInt32(txtcantidad.Text) == 0 || Convert.ToDecimal(lblsubtotal.Text) == 0)
            {
                Response.Write("<script language='javascript'>alert('datos vacios ingrese un valor');</script>");
            }
            else
            {
                int producto = Convert.ToInt32(txtproducto.Text);
                int cant = Convert.ToInt32(txtcantidad.Text);
                decimal subtotal = Convert.ToDecimal(lblsubtotal.Text);
                VentaController ven = new VentaController();
                if (ven.nuevoDetalle(factura, producto, cant, subtotal) > 0)
                {
                    //Response.Write("<script language='javascript'>alert('ok');</script>");
                    LoadDetalle(fact);
                }
                else
                {
                    Response.Write("<script language='javascript'>alert('Error en la venta');</script>");
                }
            }
            
        }


    }
}