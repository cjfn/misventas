﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Proveedores.aspx.cs" Inherits="WebPractica.AppCode.Views.Proveedores" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       
        
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="id"
        OnRowDataBound="OnRowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" EmptyDataText="No records has been added.">
        <Columns>
            <asp:TemplateField HeaderText="id" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtid" runat="server" Text='<%# Eval("id") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="nombre" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblnombre" runat="server" Text='<%# Eval("nombre") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtNombre" runat="server" Text='<%# Eval("nombre") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="direccion" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lbldireccion" runat="server" Text='<%# Eval("direccion") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtDireccion" runat="server" Text='<%# Eval("direccion") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="telefono" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lbltelefono" runat="server" Text='<%# Eval("telefono") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtTelefono" runat="server" Text='<%# Eval("telefono") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="email" ItemStyle-Width="150">
                <ItemTemplate>
                    <asp:Label ID="lblemail" runat="server" Text='<%# Eval("email") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtEmail" runat="server" Text='<%# Eval("email") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150"/>
        </Columns>
        </asp:GridView>
        <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
        <tr>
            <td style="width: 150px">
                Nombre:<br />
                <asp:TextBox ID="txtNombre" runat="server" Width="140" />
            </td>
            <td style="width: 150px">
                Direccion:<br />
                <asp:TextBox ID="txtDireccion" runat="server" Width="140" />
            </td>
            <td style="width: 150px">
                Telefono:<br />
                <asp:TextBox ID="txtTelefono" runat="server" Width="140" />
            </td>
            <td style="width: 150px">
                Email:<br />
                <asp:TextBox ID="txtEmail" runat="server" Width="140" />
            </td>

            <td style="width: 100px">
                <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Insert" />
            </td>
        </tr>
        </table>
       </div>
    </form>
</body>
</html>
